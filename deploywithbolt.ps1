#Variaveis que devem ser alteradas
$dir = "C:\instpuppet" #Diretório onde os arquivos devem ser colocados
$user = "Administrator" #Nome do usuário com privelégio de ADM em todos os pcs
$pass = '*h3e8b4a7r4a*' #Senha do usuario
$csvdata= Import-Csv -Path "$dir\host.csv" -Delimiter ';'

#Verifica se já existe instalação do Bolt
if (test-path "C:\Program Files\Puppet Labs\Bolt\bin\bolt.bat")

{
   Write-Output Bolt instalado
}

else
{
    Set-Location $dir
    Invoke-WebRequest "https://downloads.puppet.com/windows/puppet5/puppet-bolt-x64-latest.msi" -OutFile "bolt.msi"
    Start-Process msiexec.exe -Wait -ArgumentList "/I $dir\bolt.msi /quiet"
}

#Executar instalacao do puppet em varios servidores windows
ForEach ($row in $csvdata)
{
    $cabecalho = 'cert;etiqueta'
    $newcsv = "$($row.cert);$($row.etiqueta)"
    $cabecalho > $dir\temp.csv
    $newcsv >> $dir\temp.csv

    #Instala remotamente via bolt
    bolt file upload $dir\temp.csv 'c:\windows\temp\temp.csv' --nodes winrm://$($row.etiqueta) --user $user --password $pass --debug --no-ssl;
    bolt script run $dir\install.ps1 --nodes $($row.etiqueta)  --transport winrm --user $user --password $pass --debug --no-ssl;
    Remove-Item $dir\temp.csv
    
} 