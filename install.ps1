#Variáveis que devem ser alteradas
$master = "pmaster.vtainfo.local" #Endereço do puppet master
$dir = "C:\Windows\Temp"
$csvdata= Import-Csv -Path "$dir\temp.csv" -Delimiter ';'

#Download Puppet Agent
$win64 = "https://downloads.puppetlabs.com/windows/puppet5/puppet-agent-x64-latest.msi"
$win32 = "https://downloads.puppetlabs.com/windows/puppet5/puppet-agent-x86-latest.msi"

#Verifica se já existe instalação do Puppet
if (test-path "C:\Program Files\Puppet Labs\Puppet\sys\ruby\bin\ruby.exe")
{
    Write-Information "Puppet Instalado saindo do script"
    exit
}

#Verifica arquteitura do SO e faz o Download
$ostype = (Get-WmiObject win32_operatingsystem | Select-Object osarchitecture).osarchitecture

if ($ostype -eq "64 bits" -or $ostype -eq "64-bits" -or $ostype -eq "64-bit")
{
    Set-Location "c:\windows\temp"
    Invoke-WebRequest $win64 -OutFile "puppet.msi"
}
else
{
    Set-Location "c:\windows\temp"
    Invoke-WebRequest $win32 -OutFile "puppet.msi"
}

#Instalado o puppet

ForEach ($row in $csvdata)
{

    Start-Process msiexec.exe -Wait -ArgumentList "/qn /norestart /i c:\windows\temp\puppet.msi PUPPET_MASTER_SERVER=$master PUPPET_AGENT_CERTNAME=$($row.cert)"
         
}